using Godot;
using System;
using System.Linq;

public class CameraControl : ClippedCamera
{
    private KinematicBody player;

    [Export]
    private float interval = 0.5f;
    private float runner = 0f;

    private Spatial lWorld;
    private Spatial sWorld;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        player = GetNode<KinematicBody>("..");
        lWorld = GetNode<Spatial>("/root/World/LightWorld");
        sWorld = GetNode<Spatial>("/root/World/ShadowWorld");
    }

    public override void _PhysicsProcess(float delta)
    {
        if (runner >= interval)
        {
            runner = 0;
            var spaceState = GetWorld().DirectSpaceState;
            var own = ToGlobal(Vector3.Zero);
            var target = player.ToGlobal(Vector3.Zero);
            var intersect = spaceState.IntersectRay(own, target, new Godot.Collections.Array { player, this, sWorld, lWorld });
            if (intersect.Count > 0)
            {
                var pos = (Vector3)intersect["position"];
                var dist = own.DistanceTo(pos) + 0.2f;
                //Near = dist;
            }
        }
        runner += delta;
    }
}
