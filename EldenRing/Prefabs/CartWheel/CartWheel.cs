using Godot;

public class CartWheel : StaticBody
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        var area = GetNode("CollectionArea");
        area.Connect("body_shape_entered", this, "CollectCartWheel");

    }


    public void CollectCartWheel(RID body_id, Node body, int body_shape, int area_shape)
    {
        if (body.Name == "Character")
        {
            GetNode<EldenRing.Prefabs.World>("/root/World").RingCollected();

            QueueFree();
        }
    }
}
