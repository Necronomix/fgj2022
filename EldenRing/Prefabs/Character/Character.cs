using Godot;
using System;

public class Character : KinematicBody
{
    private Vector3 targetMovement;
    [Export]
    private float speed = 10;
    [Export]
    private float jumpPower = 12f;
    [Export]
    private float dambening = 0.95f;
    [Export]
    private float dambThreshold = 0.1f;
    [Export]
    private Vector3 gravity = new Vector3(0, -9.81f, 0);
    [Export]
    private int MaxJumps = 2;
    [Export]
    private float rotationSpeed = 10;
    [Export]
    private float jumpPowerTime = 0.2f;

    private int jumpsLeft;
    private ulong isInAirCounter = 0;
    private float jumpPowerTimeLeft = 0.0f;
    private Transform playerModelTargetTransform;

    Vector3 upInWorld = new Vector3(0, 1, 0);

    private AnimationPlayer animPlayer;
    private Spatial playerModelSpatial;
    private readonly string idleAnimation = "idle-loop";
    private readonly string runAnimation = "run-loop";
    private readonly string fallAnimation = "fall-loop";
    private readonly string jumpAnimation = "jump";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        jumpsLeft = MaxJumps;
        animPlayer = GetNode<AnimationPlayer>("KnightModel/KnightModel2/AnimationPlayer");
        playerModelSpatial = GetNode<Spatial>("KnightModel");
        playerModelTargetTransform = playerModelSpatial.Transform.LookingAt(playerModelSpatial.ToGlobal(Vector3.Forward), upInWorld);
    }

    public override void _Input(InputEvent inputEvent)
    {
        if (jumpsLeft > 0 && Input.IsActionJustPressed("jump", true))
        {
            targetMovement.y = jumpPower;
            jumpsLeft--;
            isInAirCounter = 10;
            jumpPowerTimeLeft = jumpPowerTime;
            animPlayer.Play(jumpAnimation); // Force jump animation regardless of current animation
            animPlayer.Seek(0.0f, true); // Start animation at beginning
        }
    }


    public override void _PhysicsProcess(float delta)
    {
        targetMovement = MoveAndSlide(targetMovement, upInWorld, true, 4, 0.785f, true);

        if (IsOnCeiling())
        {
            jumpPowerTimeLeft = 0.0f;
        }
        else if (jumpPowerTimeLeft > 0.0f)
        {
            jumpPowerTimeLeft = Mathf.Max(0.0f, jumpPowerTimeLeft - delta);
        }

        bool isOnFloor = IsOnFloor();

        if (!isOnFloor && jumpPowerTimeLeft == 0.0f)
        {
            targetMovement += gravity;
            isInAirCounter++;
        }
        else if (isOnFloor)
        {
            targetMovement = targetMovement * dambening;
            jumpsLeft = MaxJumps;
            isInAirCounter = 0;

            // This might reduce jittering on ground
            Vector3 translation = Translation;
            translation.y = Mathf.Stepify(translation.y, 0.1f);
            Translation = translation;
        }
        // if (targetMovement.Length() <= dambThreshold)
        // {
        //     targetMovement = new Vector3();
        // }
    }

    public override void _Process(float delta)
    {
        bool isRunning = false;
        var target = targetMovement;

        var moveVector = new Vector3();
        if (Input.IsActionPressed("right", true))
        {
            moveVector += Transform.basis.x;
            isRunning = true;
        }
        if (Input.IsActionPressed("left", true))
        {
            moveVector += -Transform.basis.x;
            isRunning = true;
        }
        if (Input.IsActionPressed("down", true))
        {
            moveVector += Transform.basis.z;
            isRunning = true;
        }
        if (Input.IsActionPressed("up", true))
        {
            moveVector += -Transform.basis.z;
            isRunning = true;
        }
        if (moveVector != Vector3.Zero)
        {
            target = moveVector.Normalized() * speed;
            var globalPos = playerModelSpatial.GlobalTransform.origin;
            var lookAtPos = playerModelSpatial.GlobalTransform.origin + moveVector;
            var wtransform = playerModelSpatial.GlobalTransform.LookingAt(new Vector3(lookAtPos.x, globalPos.y, lookAtPos.z), new Vector3(0, 1, 0));
            var wrotation = new Quat(playerModelSpatial.GlobalTransform.basis).Normalized().Slerp(new Quat(wtransform.basis).Normalized(), delta * 10.0f).Normalized();
            playerModelSpatial.GlobalTransform = new Transform(new Basis(wrotation), GlobalTransform.origin);
        }
        targetMovement.x = target.x;
        targetMovement.z = target.z;

        if (Input.IsActionPressed("cRotRight", true))
        {
            Rotate(upInWorld, rotationSpeed * delta);
        }
        if (Input.IsActionPressed("cRotLeft", true))
        {
            Rotate(upInWorld, -rotationSpeed * delta);
        }

        if (isInAirCounter <= 3 && jumpPowerTimeLeft == 0.0f)
        {
            SetAnimation(isRunning ? runAnimation : idleAnimation);
        }
        else if (animPlayer.AssignedAnimation != jumpAnimation)
        {
            SetAnimation(fallAnimation);
        }
    }

    private void SetAnimation(string animationName)
    {
        if (animPlayer.AssignedAnimation != animationName)
        {
            animPlayer.Play(animationName);
        }
    }
}
