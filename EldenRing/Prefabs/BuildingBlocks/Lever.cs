using Godot;
using System;

public class Lever : Spatial
{
    public bool currentlyOn = false;

    [Export]
    private string[] pathToInteractables = new string[0];

    private Spatial leverObject;
    private float targetAngle = -45f;
    private float currentAngle = -45f;

    public override void _Ready()
    {
        var area = GetNode("Area");
        area.Connect("body_shape_entered", this, "PlayerHitLever");

        leverObject = GetNode<Spatial>("lever");
    }

    public void PlayerHitLever(RID body_id, Node body, int body_shape, int area_shape)
    {
        if (body.Name != "Character")
        {
            return;
        }

        currentlyOn = !currentlyOn;
        targetAngle = currentlyOn ? 45f : -45f;

        foreach (var path in pathToInteractables)
        {

            GetNode<IReactableToLever>(path)?.LeverPulled(currentlyOn);
        }
    }

    public override void _Process(float delta)
    {
        currentAngle += (targetAngle - currentAngle) * 1f * delta;
        leverObject.RotationDegrees = new Vector3(currentAngle, 0f, 0f);
    }
}

interface IReactableToLever
{
    void LeverPulled(bool isOn);
}
