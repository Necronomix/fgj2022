﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EldenRing.Prefabs.BuildingBlocks
{
    internal class Openable : Spatial, IReactableToLever
    {
        [Export]
        private Vector3 openingTarget = new Vector3(0, 2, 0);
        [Export]
        private bool isOpen = false;
        [Export]
        private float timeToOperate = 1f;
        private Vector3 original;
        private float timer = 0f;

        public override void _Ready()
        {
            original = Transform.origin;
        }

        public void LeverPulled(bool isOn)
        {
            isOpen = !isOpen;
            timer = 0f;
        }

        public override void _PhysicsProcess(float delta)
        {
            timer = Math.Min(timer + delta, timeToOperate);
            switch (isOpen)
            {
                case false:
                    Translation = (original + openingTarget) - openingTarget * (timer / timeToOperate);
                    break;
                case true:
                    Translation = original + openingTarget * (timer / timeToOperate);
                    break;
            }
        }
    }
}
