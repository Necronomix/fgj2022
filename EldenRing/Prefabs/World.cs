﻿using Godot;
using System;


namespace EldenRing.Prefabs
{
    internal class World : Spatial
    {
        [Export]
        private float resetHeight = 3;

        public int Rings { get; set; } = 0;

        private Stage currentStage = Stage.Light;

        private Node light;
        private Node shadow;
        private Spatial character;
        private RichTextLabel ringLabel;
        

        private enum Stage
        {
            Light,
            Shadow
        }

        public override void _Ready()
        {
            light = GetNode("LightWorld");
            shadow = GetNode("ShadowWorld");
            character = GetNode("Character") as Spatial;
            ringLabel = GetNode("UI/RingLabel") as RichTextLabel;
            RemoveChild(shadow);
        }

        public override void _Input(InputEvent inputEvent)
        {
            if (inputEvent.IsActionPressed("morphWorld"))
            {
                switch (currentStage)
                {
                    case Stage.Light:
                        RemoveChild(light);
                        AddChild(shadow);
                        currentStage = Stage.Shadow;
                        character.Translation = ResetTransform(character.Translation);
                        break;
                    case Stage.Shadow:
                        RemoveChild(shadow);
                        AddChild(light);
                        currentStage = Stage.Light;
                        character.Translation = ResetTransform(character.Translation);
                        break;
                    default:
                        throw new Exception("No perkele");
                }                
            }
        }

        public Vector3 ResetTransform(Vector3 transform)
        {
            transform.y = Math.Max(resetHeight, transform.y);
            return transform;
        }

        public void RingCollected()
        {
            Rings++;
            ringLabel.Text = $"{Rings}";

            // The amount of sin that has been made here is amazing
            for (int i = 1; i <= 20; i++)
            {
                Spatial cartwheel = GetNode<Spatial>("/root/World/Character/KnightModel/KnightModel2/KnightArmature/Skeleton/BoneAttachmentHead/CartwheelOnHead-" + i);
                if (cartwheel != null)
                {
                    cartwheel.Visible = i <= Rings;
                }
            }
        }
    }
}
