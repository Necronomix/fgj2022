using Godot;
using System;

public class CartwheelOnHead : Spatial
{
    private Spatial parent;

    private int nodeIndex = 0;
    private int bufferIndex = 0;
    private Godot.Collections.Array<Transform> buffer;

    private int bufferIncrease = 1;
    private float cartwheelHeight = 0.3f;

    public override void _Ready()
    {
        nodeIndex = int.Parse(Name.Split('-')[1]);
        int bufferSize = Mathf.Max(1, (int)(nodeIndex * bufferIncrease * 0.7f));

        parent = GetParent<Spatial>();
        buffer = new Godot.Collections.Array<Transform>();

        for (int i = 0; i < bufferSize; i++)
        {
            buffer.Add(parent.GlobalTransform);
        }

        ProcessPriority = 1000000;
    }

    public override void _PhysicsProcess(float delta)
    {
        Transform temp = parent.GlobalTransform;
        temp.origin = parent.ToGlobal(new Vector3(0, cartwheelHeight * (nodeIndex - 1), nodeIndex * -0.08f));
        buffer[bufferIndex] = temp;

        GlobalTransform = buffer[(bufferIndex + 1) % buffer.Count];

        Transform temp2 = GlobalTransform;
        temp2.origin.y = (temp2.origin.y + temp.origin.y) * 0.5f;
        GlobalTransform = temp2;

        bufferIndex++;
        if (bufferIndex >= buffer.Count)
        {
            bufferIndex = 0;
        }
    }
}
