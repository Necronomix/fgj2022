using Godot;
using System;

public class Rotate : Spatial
{
    [Export]
    public Vector3 axis = new Vector3(0, 1, 0);

    [Export]
    public float speed = 1.0f;
    private Vector3 normalized;

    public override void _Ready()
    {
        normalized = axis.Normalized();
    }

    public override void _Process(float delta)
    {
        Rotate(normalized, speed * delta);
    }
}
