# README #

Simo and Tero participated in FGJ2022 with a bit of anticipation for a new game coming in almost months time.
Can you guess what that game is?

We also wanted to learn Godot + C# so it was a learning opportunity for us.
There may be solutions that are suboptimal due to time limit and limited godot understanding!
### What is this repository for? ###

* FGJ2022
* 0.1.0

### How do I get set up? ###

* Godot 3.4.2
* C# editor
* .NET Core 3.1 installed

### Contribution guidelines ###

* Project very likely will not be continued

### Who do I talk to? ###

* You can contact repo owner in case there is anything important